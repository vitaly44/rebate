$(document).ready(function () {
    var _hideNavMenu = $('#btn_hideNavMenu'),
        _showNavMenu = $('#btn_showNavMenu'),
        _headerNav = $('#header_nav');

    _hideNavMenu.on('click', function () {
        _headerNav.fadeOut();
    });
    _showNavMenu.on('click', function () {
        _headerNav.fadeIn();
    });
});

$(document).ready(function () {
    var _sectionCount = $('.section').length,
        _footerNav = $('#footer_nav'),
        _footerNavItem = $('.footer_nav-item');

    _footerNavItem.remove();


    for (var i = 0; i < _sectionCount; i++) {
        $(_footerNavItem[0]).clone().appendTo(_footerNav);
    }
});

$(document).ready(function () {
    var _faqQ = $('.faq_item-q');
    var _faqA = $('.faq_item-a');

    _faqQ.on('click', function () {
        var
            _this = $(this),
            _thisAns = _this.next(),
            _thisShow = _this.hasClass('open');

        _faqQ.removeClass('open');
        _faqA.removeClass('open');

        if (_thisShow) {
            _this.removeClass('open');
            _thisAns.removeClass('open');
        } else {
            _this.addClass('open');
            _thisAns.addClass('open');
        };
    })
});

$(document).ready(function () {
    $('html, body').on('click', function () {
        var eventInMenu = $(event.target).parents('.header_wrap');

        if (!eventInMenu.length) {
            $("#header_nav").fadeOut();
        }
    })
})
$(document).ready(function () {
    $('.section').scroll(function () {
        $("#header_nav").fadeOut();
    })
})

$(document).ready(function () {
    $('#pagepiling').pagepiling({
        verticalCentered: false,
        scrollingSpeed: 400,
        navigation: false,
        easing: 'linear',
        afterRender: function (anchorLink, index) {
            $('.section').eq(0).find('.section_title').fadeIn(300);
        },
        afterLoad: function (anchorLink, index) {
            $('.section').eq(index - 1).find('.section_title').fadeIn(300);
        },
        onLeave: function (index, nextIndex, direction) {
            $("#header_nav").fadeOut();

            $('.f_item_current').html('0' + (nextIndex));

            $('.footer_nav-item').eq(index).removeClass('footer_nav-item--active');

            $('.footer_nav-item').removeClass('footer_nav-item--active');
            $('.footer_nav-item').eq(nextIndex - 1).addClass('footer_nav-item--active');
        },
    });

    $('.footer_nav-item').on('click', function (e) {
        var _this = $(this);

        $.fn.pagepiling.moveTo(_this.index());
    })
});


$(document).ready(function () {
    $('.section_content_video-img').on('click', function () {
        var
            _this = $(this),
            _video = $('.section_content_video-video');

        _this.hide();

        _video.addClass('play');

        _video[0].play();
    })
    $('.section_content_video-video').on('click', function () {
        var
            _this = $(this),
            _video = _this[0];

        if (_video.paused) {
            _video.play();
            _this.addClass('play');

        } else {
            _video.pause();
            _this.removeClass('play');
        };
    })
});