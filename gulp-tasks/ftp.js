module.exports = function (gulp, plugins, path) {
	return () => {

		function getConn() {
			return plugins.ftp.create({
				host: 'rebate.artbayard.ru',
				user: 'rebate.artbayard.ru|ftp_rebate',
				pass: '1234'
			});
		}

		var conn = getConn();

		return gulp.src(path.public.all)
			.pipe(conn.dest('./'));
	}
}