module.exports = function (gulp, plugins, path) {
	return () => {
		return gulp.src([path.public.all, path.src.scss.all], {
				base: '.'
			})
			.pipe(plugins.plumber({
				errorHandler: plugins.notify.onError()
			}))
			.pipe(plugins.zip('sources.zip'))
			.pipe(gulp.dest(path.public.root));
	}
}